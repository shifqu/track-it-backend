import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class Profile(AbstractUser):
    website = models.URLField(
        _('website'),
        null=True,
        blank=True
    )

    created_at = models.DateTimeField(
        _('created at'),
        auto_now=True,
        editable=False
    )
