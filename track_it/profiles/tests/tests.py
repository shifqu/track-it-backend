from django.test import TestCase

from . import factories


class ProfileModelTest(TestCase):
    def setUp(self):
        self.p = factories.ProfileFactory()

    def test_profile_creation(self):
        assert self.p.pk > 0
