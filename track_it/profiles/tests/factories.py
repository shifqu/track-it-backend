from django.contrib.auth import get_user_model

import factory


class ProfileFactory(factory.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda i: 'f_user{0}'.format(str(i)))
    password = factory.Faker('password')
    website = factory.Faker('url')
