import graphene
from base.graphene.types import BaseDjangoObjectType
from base.graphene.fields import BaseField, BaseList
from base.graphene.mutations import ModelMutation, DeleteModelMutation

from . import models


FILTER_FIELDS = {
    'name':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
    'company_name':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
    'phone':
        (graphene.String(), ['exact', 'startswith']),
    'vat_number':
        (graphene.String(), ['iexact', 'istartswith'])
}


class ClientType(BaseDjangoObjectType):
    class Meta:
        model = models.Client
        field_filters = FILTER_FIELDS


class Query(object):
    client = BaseField(
        ClientType
    )
    clients = BaseList(
        ClientType
    )


class CreateClientMutation(ModelMutation):
    class Meta:
        model = models.Client
        filter_fields = FILTER_FIELDS
        output = ClientType


class UpdateClientMutation(ModelMutation):
    class Meta:
        model = models.Client
        filter_fields = FILTER_FIELDS
        output = ClientType


class DeleteClientMutation(DeleteModelMutation):
    class Meta:
        model = models.Client


class Mutation(object):
    create_client = CreateClientMutation.Field()
    update_client = UpdateClientMutation.Field()
    delete_client = DeleteClientMutation.Field()
