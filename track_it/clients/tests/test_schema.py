from graphene.test import Client
from django.test import TestCase
from graphql_relay.node.node import to_global_id, from_global_id
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory

from . import factories
from .. import models
from ..schema import ClientType
from track_it.schema import schema
from profiles.tests.factories import ProfileFactory


class ClientSchemaTest(TestCase):
    def setUp(self):
        # Set up a user and create clients for it
        self.user = ProfileFactory()
        self.set_up_clients_for(self.user)

        # Create a second user to test empty results
        self.user2 = ProfileFactory()

        # Set up graphene.test.Client and request
        self.client = Client(schema)
        self.request = RequestFactory().get('/')

        # Convenient access to one specific client
        self.client7_rid = to_global_id('Client', self.client7.pk)
        self.c_name = self.client7.name

    def set_up_clients_for(self, user):
        """ Create some users with their profile set to user """
        for x in range(1, 11):
            # Create some users with their profile set to self.user
            setattr(self, 'client{}'.format(str(x)), factories.ClientFactory())
            curr_client = getattr(self, 'client{}'.format(str(x)))
            curr_client.profile = user
            curr_client.save()

    def log_in(self, user):
        """ Log in a user """
        self.request.user = user
        self.client.user = user

    @staticmethod
    def test_client_type():
        """ Test the initialization of the ClientType"""
        instance = ClientType()
        assert instance

    def test_singular_anon(self):
        """Testing the ClientNode for singular result with AnonymousUser"""
        self.log_in(AnonymousUser())

        result = self.client.execute('''
        {
          client(id: "%s") {
            id
            name
          }
        }
        ''' % (self.client7_rid,), context_value=self.request)
        assert not result['data']['client']

    def test_singular_authenticated(self):
        """Testing the ClientNode for singular result with authenticated user"""
        self.log_in(self.user)

        result = self.client.execute('''
        {
          client(id: "%s") {
            id
            name
          }
        }
        ''' % (self.client7_rid,), context_value=self.request)
        assert result['data']['client']

        result = self.client.execute('''
        {
          client(name: "%s") {
            id
            name
          }
        }
        ''' % (self.c_name,), context_value=self.request)
        assert result['data']['client']

    def test_singular_empty_result(self):
        """Testing the ClientNode for singular result with another authenticated user"""
        self.log_in(self.user2)

        result = self.client.execute('''
        {
          client(id: "%s") {
            id
            name
          }
        }
        ''' % (self.client7_rid,), context_value=self.request)
        assert not result['data']['client']

        result = self.client.execute('''
        {
          client(name: "%s") {
            id
            name
          }
        }
        ''' % (self.c_name,), context_value=self.request)
        assert not result['data']['client']

    def test_plural_anon(self):
        """Testing the ClientNode for plural result with AnonymousUser"""
        self.log_in(AnonymousUser())

        result = self.client.execute('''
        {
          clients {
            id
            name
          }
        }
        ''', context_value=self.request)
        assert not result['data']['clients']

    def test_plural_authenticated(self):
        """Testing the ClientNode for plural result with authenticated user"""
        self.log_in(self.user)

        result = self.client.execute('''
        {
          clients {
            id
            name
          }
        }
        ''', context_value=self.request)
        assert result['data']['clients']

        result = self.client.execute('''
        {
          clients(name_Icontains: "%s") {
            id
            name
          }
        }
        ''' % (self.c_name, ), context_value=self.request)
        assert result['data']['clients']

    def test_plural_empty_result(self):
        """Testing the ClientNode for plural result with another authenticated user"""
        self.log_in(self.user2)

        result = self.client.execute('''
        {
          clients {
            id
            name
          }
        }
        ''', context_value=self.request)
        assert not result['data']['clients']

        result = self.client.execute('''
        {
          clients(name_Icontains: "%s") {
            id
            name
          }
        }
        ''' % (self.c_name,), context_value=self.request)
        assert not result['data']['clients']

    def test_create_client_mutation(self):
        """ Test creating a client """
        query = '''
            mutation {
              createClient(name: "KBC mechelen", companyName: "KBC") {
                  id
                  name
                  companyName
                  createdAt
                  projects {
                    edges {
                      node {
                        id
                        createdAt
                      }
                    }
                  }
              }
            }
         '''

        # Check with anon
        self.log_in(AnonymousUser())
        executed = self.client.execute(query, context_value=self.request)
        assert not executed['data']['createClient']

        # Check for success
        self.log_in(self.user)
        executed = self.client.execute(query, context_value=self.request)
        assert executed['data']['createClient']

        # Check for missing arg
        executed = self.client.execute('''
            mutation {
              createClient(companyName: "KBC") {
                  id
                  name
                  companyName
                  createdAt
                  projects {
                    edges {
                      node {
                        id
                        createdAt
                      }
                    }
                  }
              }
            }
        ''', context_value=self.request)
        assert not executed['data']['createClient']

    def test_update_client_mutation(self):
        """ Test updating a client """
        self.log_in(self.user)

        qry = '''
            mutation {
              updateClient(id: "%s", name: "%s", companyName: "%s") {
                  id
                  name
                  companyName
                  createdAt
                  projects {
                    edges {
                      node {
                        id
                        createdAt
                      }
                    }
                  }
              }
            }
        ''' % (self.client7_rid, 'Jaques', 'lentreprise')
        result = self.client.execute(qry, context_value=self.request)
        # Get the object from the db, since this test returns a different
        # encoded ID; but for this update we need to make sure that we still
        # have the same object and that we didn't get a newly created obj.
        id_ = from_global_id(result['data']['updateClient'].pop('id'))[1]
        c = models.Client.objects.get(id=id_)
        assert c == self.client7
        assert result['data']['updateClient']['name'] == 'Jaques'
        assert result['data']['updateClient']['companyName'] == 'lentreprise'

    def test_update_client_mutation_unauthorized_user(self):
        """ Test updating a client with an unauthorized user """
        self.log_in(self.user2)

        qry = '''
            mutation {
              updateClient(id: "%s", name: "%s", companyName: "%s") {
                  id
                  name
                  companyName
                  createdAt
                  projects {
                    edges {
                      node {
                        id
                        createdAt
                      }
                    }
                  }
              }
            }
        ''' % (self.client7_rid, 'Jaques', 'lentreprise')
        result = self.client.execute(qry, context_value=self.request)
        assert not result['data']['updateClient']

    def test_delete_client_mutation(self):
        """ Test deleting a client """
        self.log_in(self.user)

        qry = '''
            mutation {
              deleteClient(id: "%s") {
                status
              }
            }
        ''' % (self.client7_rid, )
        result = self.client.execute(qry, context_value=self.request)
        assert result['data']['deleteClient']['status'] == 200

        # Test missing args
        qry = '''
            mutation {
              deleteClient {
                status
              }
            }
        '''
        result = self.client.execute(qry, context_value=self.request)
        assert not result['data']['deleteClient']

    def test_delete_client_mutation_anon(self):
        """ Test deleting a client anonymously """
        self.log_in(AnonymousUser())
        qry = '''
            mutation {
              deleteClient(id: "%s") {
                status
              }
            }
        ''' % (self.client7_rid, )
        result = self.client.execute(qry, context_value=self.request)
        assert not result['data']['deleteClient']

    def test_delete_client_mutation_other_user(self):
        """ Test deleting a client from user1 with a session of user2 """
        self.log_in(self.user2)
        qry = '''
            mutation {
              deleteClient(id: "%s") {
                status
              }
            }
        ''' % (self.client7_rid, )
        result = self.client.execute(qry, context_value=self.request)
        assert not result['data']['deleteClient']
