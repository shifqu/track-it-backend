from django.test import TestCase
from django.db.utils import IntegrityError

from . import factories
from ..models import Client


class ClientModelTest(TestCase):
    def setUp(self):
        self.c = factories.ClientFactory()

    def test_client_creation(self):
        assert self.c.pk > 0
        assert self.c.profile.pk > 0

    def test_client_str_representation(self):
        assert isinstance(str(self.c), str)

    def test_client_name_profile_unique_together(self):
        assertion_result = False
        try:
            c = Client()
            c.name = self.c.name
            c.profile = self.c.profile
            c.save()
        except IntegrityError:
            assertion_result = True

        assert assertion_result


