import factory


class ClientFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'clients.Client'

    name = factory.Faker('name')
    company_name = factory.Faker('name')
    phone = factory.Faker('text', max_nb_chars=25)

    profile = \
        factory.SubFactory('profiles.tests.factories.ProfileFactory')
