from django.db import models
from django.utils.translation import gettext_lazy as _

from vies.models import VATINField

from base.models import BaseModel


class Client(BaseModel):
    name = models.CharField(
        _('name'),
        max_length=30,
        blank=False,
    )
    company_name = models.CharField(
        _('company name'),
        max_length=30,
        blank=True,
        default='',
    )
    phone = models.CharField(
        _('phone'),
        max_length=25,
        blank=True,
        default='',
    )
    vat_number = VATINField(
        _('EU VAT ID'),
        blank=True,
        default='',
    )

    class Meta:
        unique_together = ('profile', 'name')

    def __str__(self):
        return self.name

# FIXME improve phone field, eg make 2 fields, zone and number, add formatting [hint: filters]
