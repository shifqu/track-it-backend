from django.db import models
from django.utils.translation import gettext_lazy as _

from base.models import BaseModel


class Project(BaseModel):
    name = models.CharField(
        _('name'),
        max_length=50,
        null=False,
        blank=False,
    )
    description = models.TextField(
        _('description'),
        max_length=100,
        null=False,
        blank=True,
        default='',
    )
    client = models.ForeignKey(
        'clients.Client',
        on_delete=models.CASCADE,
        related_name='projects',
        related_query_name='project',
        null=True,
        blank=True,
    )

    class Meta:
        unique_together = ('profile', 'name')

    def __str__(self):
        return self.name


class Activity(BaseModel):
    name = models.CharField(
        _('name'),
        max_length=50,
    )
    description = models.TextField(
        _('description'),
        max_length=100,
        null=False,
        blank=True,
        default='',
    )
    project = models.ForeignKey(
        'Project',
        on_delete=models.CASCADE,
        related_name='activities',
        related_query_name='activity',
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = ('profile', 'name', 'project')

    def __str__(self):
        return self.name


class TimeSpan(BaseModel):
    start = models.DateTimeField(
        _('start'),
        auto_now=True,
    )
    end = models.DateTimeField(
        _('end'),
        blank=True,
        null=True,
    )
    activity = models.ForeignKey(
        'Activity',
        on_delete=models.CASCADE,
        related_name='timespans',
        related_query_name='timespan',
    )

    def __str__(self):
        return ' | '.join(map(str, [self.start, self.end, self.activity]))
