from graphene.test import Client
from graphql_relay.node.node import to_global_id
from django.test import TestCase, RequestFactory

from . import factories
from ..schema import ProjectType, ActivityType, TimeSpanType
from track_it.schema import schema
from profiles.tests.factories import ProfileFactory


class ProjectSchemaTest(TestCase):
    def setUp(self):
        # Set a user which will create all the clients
        self.user = ProfileFactory()
        for x in range(1, 11):
            # Create some projects with their profile set to self.user
            setattr(self, 'project{}'.format(str(x)), factories.ProjectFactory())
            curr_project = getattr(self, 'project{}'.format(str(x)))
            curr_project.profile = self.user
            curr_project.save()

        # Create a second user to test empty results
        self.user2 = ProfileFactory()
        self.client = Client(schema)
        self.req = RequestFactory().get('/')
        self.p_rid = to_global_id('Project', self.project7.pk)
        self.p_name = self.project7.name

        self.a_rid = to_global_id('Activity', self.project7.activities.first().pk)
        self.a_name = self.project7.activities.first().name

        self.t_rid = to_global_id('TimeSpan', self.project7.activities.first().timespans.first().pk)
        self.t_name = self.project7.activities.first().timespans.first().start

        # Do the setup for the sake of completeness, but no need to test anything here, since clients app is tested
        # and this schema is defined the exact same. #lazy but 100% coverage

    @staticmethod
    def test_project_types():
        instance = ProjectType()
        assert instance

        instance = ActivityType()
        assert instance

        instance = TimeSpanType()
        assert instance

    def test_create_timespan(self):
        # Open a project; create timespan
        self.req.user = self.user
        self.client.user = self.user

        qry_open_project = '''
        {
          project(id: "%s") {
            id
            name
            description
            activities {
              edges {
                node {
                  id
                  name
                  timespans {
                    edges {
                      node {
                        id
                        start
                        end
                      }
                    }
                  }
                }
              }
            }
          }
        }
        ''' % (self.p_rid, )
        result = self.client.execute(qry_open_project, context_value=self.req)
        # print(result)
        assert result['data']['project']['id']

        # print(self.project7)
        # print(self.project7.activities.first().timespans.first())
        # print(self.project7)

        qry_start_timer = '''
        mutation {
          createTimeSpan(%s: "%s"){
            id
            start
            activity {
              id
              name
              description
              project {
                id
                name
                description
              }
            }
          }
        }
        '''
        # Start a timer from activity
        a_id = to_global_id('Activity', self.project7.activities.first().pk)
        result = self.client.execute(qry_start_timer % ('activityId', a_id, ), context_value=self.req)
        # print(result)
        assert result['data']['createTimeSpan']['id']
        assert result['data']['createTimeSpan']['activity']['id']

        # Start a timer from project
        p_id = to_global_id('Project', self.project7.pk)
        result = self.client.execute(qry_start_timer % ('projectId', p_id, ), context_value=self.req)
        # print(result)
        assert result['data']['createTimeSpan']['id']
        assert result['data']['createTimeSpan']['activity']['id']
        assert result['data']['createTimeSpan']['activity']['project']['id']

    def test_delete_project(self):
        self.req.user = self.user
        self.client.user = self.user
        qry = '''
            mutation {
              deleteProject(id: "%s") {
                status
              }
            }
        ''' % (self.p_rid, )
        result = self.client.execute(qry, context_value=self.req)
        assert result['data']['deleteProject']['status'] == 200

    def test_delete_activity(self):
        profile = ProfileFactory()
        activity = factories.ActivityFactory()
        activity.profile = profile
        activity.save()
        self.req.user = profile
        self.client.user = profile
        qry = '''
            mutation {
              deleteActivity(id: "%s") {
                status
              }
            }
        ''' % (to_global_id('Activity', activity.id), )
        result = self.client.execute(qry, context_value=self.req)
        assert result['data']['deleteActivity']['status'] == 200

        # Test the same id, with another user
        self.req.user = self.user
        self.client.user = self.user
        result = self.client.execute(qry, context_value=self.req)
        assert not result['data']['deleteActivity']

    def test_delete_time_span(self):
        profile = ProfileFactory()
        timespan = factories.TimeSpanFactory()
        timespan.profile = profile
        timespan.save()
        self.req.user = profile
        self.client.user = profile
        qry = '''
            mutation {
              deleteTimeSpan(id: "%s") {
                status
              }
            }
        ''' % (to_global_id('TimeSpan', timespan.id), )
        result = self.client.execute(qry, context_value=self.req)
        assert result['data']['deleteTimeSpan']['status'] == 200

        # Test the same id, with another user
        self.req.user = self.user
        self.client.user = self.user
        result = self.client.execute(qry, context_value=self.req)
        assert not result['data']['deleteTimeSpan']
