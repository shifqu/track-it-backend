import factory
import random

from django.utils import timezone as tz


class TimeSpanFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'projects.TimeSpan'

    start = tz.now() + tz.timedelta(hours=random.randint(1, 100))
    end = start + tz.timedelta(hours=random.randint(1, 5))
    profile = \
        factory.SubFactory('profiles.tests.factories.ProfileFactory')
    activity = \
        factory.SubFactory('projects.tests.factories.ActivityFactory')


class ActivityFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'projects.Activity'

    name = factory.Faker('name')
    description = factory.Faker('text', max_nb_chars=100)
    profile = \
        factory.SubFactory('profiles.tests.factories.ProfileFactory')
    project = \
        factory.SubFactory('projects.tests.factories.ProjectFactory')
    timespans = factory.RelatedFactory(TimeSpanFactory, 'activity')


class ProjectFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'projects.Project'

    name = factory.Faker('name')
    description = factory.Faker('text', max_nb_chars=100)
    profile = \
        factory.SubFactory('profiles.tests.factories.ProfileFactory')
    activities = factory.RelatedFactory(ActivityFactory, 'project')
