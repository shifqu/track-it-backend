from django.test import TestCase
from django.db.utils import IntegrityError

from . import factories
from ..models import Project, Activity


class ProjectModelTest(TestCase):
    def setUp(self):
        self.p = factories.ProjectFactory()

    def test_project_creation(self):
        assert self.p.pk > 0
        assert self.p.profile.pk > 0
        assert self.p.activities.count() > 0
        assert self.p.activities.first().timespans.count() > 0

    def test_project_str_representation(self):
        assert isinstance(str(self.p), str)
        assert isinstance(str(self.p.activities.first()), str)
        assert isinstance(str(self.p.activities.first().timespans.first()), str)

    def test_project_name_profile_unique_together(self):
        assertion_result = False
        try:
            p = Project()
            p.name = self.p.name
            p.profile = self.p.profile
            p.save()
        except IntegrityError:
            assertion_result = True

        assert assertion_result

    def test_activity_name_project_profile_unique_together(self):
        assertion_result = False
        try:
            a = Activity()
            activities_first = self.p.activities.first()
            a.name = activities_first.name
            a.profile = activities_first.profile
            a.project = activities_first.project
            a.save()
        except IntegrityError:
            assertion_result = True

        assert assertion_result
