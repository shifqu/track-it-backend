# Generated by Django 2.2.dev20180918230058 on 2018-09-18 23:22

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0005_auto_20180828_1717'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='activity',
            unique_together={('profile', 'name', 'project')},
        ),
        migrations.AlterUniqueTogether(
            name='project',
            unique_together={('profile', 'name')},
        ),
    ]
