from django.contrib import admin

from .models import Project, Activity, TimeSpan


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Project)


class ActivityAdmin(admin.ModelAdmin):
    pass


admin.site.register(Activity)


class TimeSpanAdmin(admin.ModelAdmin):
    pass


admin.site.register(TimeSpan)
