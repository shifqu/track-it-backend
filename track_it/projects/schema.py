import graphene
from graphql_relay.node.node import from_global_id
from base.graphene.types import BaseDjangoObjectType
from base.graphene.fields import BaseField, BaseList
from base.graphene.mutations import ModelMutation, DeleteModelMutation, INCLUDE_LEVELS

from . import models

PROJECT_FILTER_FIELDS = {
    'name':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
    'description':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
}

ACTIVITY_FILTER_FIELDS = {
    'name':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
    'description':
        (graphene.String(), ['iexact', 'icontains', 'istartswith']),
}

TIMESPAN_FILTER_FIELDS = {
    'start':
        (graphene.DateTime(), ['gt', 'gte', 'lt', 'lte']),
    'end':
        (graphene.DateTime(), ['gt', 'gte', 'lt', 'lte']),
}


class ProjectType(BaseDjangoObjectType):
    class Meta:
        model = models.Project
        field_filters = PROJECT_FILTER_FIELDS


class ActivityType(BaseDjangoObjectType):
    class Meta:
        model = models.Activity
        field_filters = ACTIVITY_FILTER_FIELDS


class TimeSpanType(BaseDjangoObjectType):
    class Meta:
        model = models.TimeSpan
        field_filters = TIMESPAN_FILTER_FIELDS


class Query(object):
    project = BaseField(
        ProjectType
    )
    projects = BaseList(
        ProjectType
    )

    activity = BaseField(
        ActivityType
    )
    activities = BaseList(
        ActivityType
    )

    timespan = BaseField(
        TimeSpanType
    )
    timespans = BaseList(
        TimeSpanType
    )


class CreateProjectMutation(ModelMutation):
    class Meta:
        model = models.Project
        filter_fields = PROJECT_FILTER_FIELDS
        output = ProjectType


class CreateActivityMutation(ModelMutation):
    class Meta:
        model = models.Activity
        filter_fields = ACTIVITY_FILTER_FIELDS
        output = ActivityType


class CreateTimeSpanMutation(ModelMutation):
    class Meta:
        model = models.TimeSpan
        filter_fields = TIMESPAN_FILTER_FIELDS
        include_level = INCLUDE_LEVELS.SECOND
        output = TimeSpanType

    @classmethod
    def extra_handlers(cls, user, opts):
        """
        We want to be able to create a timespan by only passing a project,
        so the creation of the activity should be automated
        :param user:
        :param opts:
        :return:
        """
        try:
            p_id = from_global_id(opts['project_id'])[1]
            project = models.Project.objects.get(profile=user, pk=p_id)
            activity = models.Activity()
            activity_count = models.Activity.objects.filter(profile=user).count()
            activity.project = project
            activity.profile = user
            activity.name = '{} {}'.format(models.Activity.__name__, str(activity_count+1).zfill(4))
            activity.save()
            opts['activity_id'] = activity.pk
            return opts
        except KeyError:
            return opts


class UpdateProjectMutation(ModelMutation):
    class Meta:
        model = models.Project
        filter_fields = PROJECT_FILTER_FIELDS
        output = ProjectType


class UpdateActivityMutation(ModelMutation):
    class Meta:
        model = models.Activity
        filter_fields = ACTIVITY_FILTER_FIELDS
        output = ActivityType


class UpdateTimeSpanMutation(ModelMutation):
    class Meta:
        model = models.TimeSpan
        filter_fields = TIMESPAN_FILTER_FIELDS
        output = TimeSpanType


class DeleteProjectMutation(DeleteModelMutation):
    class Meta:
        model = models.Project


class DeleteActivityMutation(DeleteModelMutation):
    class Meta:
        model = models.Activity


class DeleteTimeSpanMutation(DeleteModelMutation):
    class Meta:
        model = models.TimeSpan


class Mutation(object):
    create_project = CreateProjectMutation.Field()
    create_activity = CreateActivityMutation.Field()
    create_time_span = CreateTimeSpanMutation.Field()
    update_project = UpdateProjectMutation.Field()
    update_activity = UpdateActivityMutation.Field()
    update_time_span = UpdateTimeSpanMutation.Field()
    delete_project = DeleteProjectMutation.Field()
    delete_activity = DeleteActivityMutation.Field()
    delete_time_span = DeleteTimeSpanMutation.Field()
