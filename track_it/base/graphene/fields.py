import binascii
import graphene
from graphql_relay.node.node import from_global_id
from .types import BaseDjangoObjectType


class BaseField(graphene.Field):

    def __init__(self, of_type, **kwargs):
        assert issubclass(of_type, BaseDjangoObjectType), (
            "The attribute of_type in {} needs to be a subclass of "
            'BaseDjangoObjectType, received "{}".'
        ).format(self.__name__, of_type)

        kwargs.update(id=graphene.ID())
        kwargs.update(
            {k: v[0] for k, v in of_type._meta.field_filters.items()}
        )
        kwargs.update(
            resolver=lambda s, a, **k: self.field_resolver(a, **k)
        )
        super(BaseField, self).__init__(of_type, **kwargs)

    def field_resolver(self, info, **kwargs):
        type__meta = self._type._meta
        info__context_user = info.context.user

        if not info__context_user.is_authenticated:
            return None

        try:
            if kwargs.get('id'):
                rid = from_global_id(kwargs['id'])
                return type__meta.model.objects.get(profile=info__context_user, id=rid[1])
            else:
                # id was not found in kwargs, start checking for other fields
                filter_args = {k: v for k, v in kwargs.items() if k in type__meta.field_filters}
                return type__meta.model.objects.get(profile=info__context_user, **filter_args)
        except (binascii.Error, type__meta.model.DoesNotExist, type__meta.model.MultipleObjectsReturned):
            return None


class BaseList(graphene.List):

    def __init__(self, of_type, *args, **kwargs):
        assert issubclass(of_type, BaseDjangoObjectType), (
            "The attribute of_type in {} needs to be a subclass of "
            'BaseDjangoObjectType, received "{}".'
        ).format(self.__name__, of_type)

        kwargs.update(of_type._meta.field_matrix)
        kwargs.update(
            resolver=lambda s, a, **k: self.list_resolver(a, **k)
        )
        super(BaseList, self).__init__(of_type, *args, **kwargs)

    def list_resolver(self, info, **kwargs):
        type__meta = self.of_type._meta
        info__context_user = info.context.user

        if not info__context_user.is_authenticated:
            return None

        filter_args = {k: v for k, v in kwargs.items() if k in type__meta.field_matrix}
        if filter_args:
            return type__meta.model.objects.filter(profile=info__context_user, **filter_args)

        return type__meta.model.objects.filter(profile=info__context_user)

# TODO: Add or-chaining functionality to filtering
