import binascii
import graphene

from collections import namedtuple
from graphene.types import mutation
from graphql_relay.node.node import from_global_id

from django.db.models.fields.related import ForeignKey

from ..models import BaseModel

_INCLUDE_LEVELS_NT = namedtuple('int', 'NONE, FIRST, SECOND')
INCLUDE_LEVELS = _INCLUDE_LEVELS_NT(0, 1, 2)


class BaseMutation(graphene.Mutation):
    status = graphene.Int()
    error_list = graphene.String()

    class Meta:
        abstract = True


class ModelMutationOptions(mutation.MutationOptions):
    model = None
    filter_fields = None  # Type: dict
    excluded_fields = None  # Type: list
    include_level = INCLUDE_LEVELS.FIRST  # Type: int


class ModelMutation(BaseMutation):
    class Meta:
        abstract = True

    @classmethod
    def __init_subclass_with_meta__(
            cls,
            model=None,
            arguments=None,
            _meta=None,
            filter_fields=None,
            excluded_fields=None,
            include_level=INCLUDE_LEVELS.FIRST,
            **options,
    ):
        assert issubclass(model, BaseModel), (
            "The attribute model in {} needs to be a subclass of "
            'BaseModel, received "{}".'
        ).format(cls.__name__, model)

        assert isinstance(filter_fields, dict), (
            "The attribute filter_fields in {} needs to be an instance of "
            'dict, received "{}".'
        ).format(cls.__name__, filter_fields)

        assert include_level in range(4), (
            "The attribute include_level in {} needs to be between 0 and 3"
            ', received "{}".'
        ).format(cls.__name__, include_level)

        if not _meta:
            _meta = ModelMutationOptions(cls)

        if not excluded_fields:
            # By default ignore the BaseModel's fields for migrations
            excluded_fields = BaseModel._meta.get_fields()

        _meta.model = model
        _meta.filter_fields = filter_fields
        _meta.excluded_fields = excluded_fields
        _meta.include_level = include_level

        if not arguments:
            # The superclass will attach the arguments to the _meta
            arguments = cls.generate_arguments(_meta)

        super().__init_subclass_with_meta__(_meta=_meta, arguments=arguments, **options)

    @classmethod
    def generate_arguments(cls, _meta):
        # Initialize arguments using the filter_fields that were passed.
        arguments = {k: v[0] for k, v in _meta.filter_fields.items()}

        # Add the id to arguments
        arguments.update(id=graphene.ID())

        # Add the relational field_names
        if _meta.include_level > INCLUDE_LEVELS.NONE:
            cls.add_relational_fields(_meta, arguments)
        return arguments

    @classmethod
    def add_relational_fields(cls, _meta, arguments):
        relation_fields = cls.get_relation_fields_by_meta(_meta)
        if _meta.include_level > INCLUDE_LEVELS.FIRST:
            cls.add_second_level_relational_fields(_meta, relation_fields)
        relation_field_dict = {cls.convert_relational_name(x): graphene.ID() for x in relation_fields}
        arguments.update(relation_field_dict)

    @classmethod
    def convert_relational_name(cls, x):
        return '_'.join([x.name.lower(), 'id'])

    @classmethod
    def get_relation_fields_by_meta(cls, meta):
        return cls.get_relation_fields(meta.model._meta.get_fields(), meta.excluded_fields)

    @classmethod
    def get_relation_fields(cls, model_fields, excluded_fields):
        relational_fields = [
            f for f in model_fields
            if f.is_relation
            and f not in excluded_fields
            and isinstance(f, ForeignKey)
        ]

        return relational_fields

    @classmethod
    def add_second_level_relational_fields(cls, _meta, fields):
        """
        :param fields: A list of relational_fields
        :return:
        """
        assert _meta.include_level == INCLUDE_LEVELS.SECOND, (
            'This method should only be used if include levels are'
            'set to INCLUDE_LEVELS.SECOND'
        )
        new_fields = []

        for x in fields:
            # I fear we won't be able to get this model's excluded fields
            # unless we create excluded fields on the model itself. Right?
            # FIXME: Currenlty we only exclude BaseModel's meta fields
            second_rel_exclude_fields = []
            if issubclass(x.related_model, BaseModel):
                second_rel_exclude_fields = BaseModel._meta.get_fields()

            second_rel_fields = cls.get_relation_fields(
                x.related_model._meta.get_fields(),
                second_rel_exclude_fields
            )

            new_rel_fields = [x for x in second_rel_fields if x not in fields]
            if new_rel_fields:
                new_fields.extend(new_rel_fields)

        fields += new_fields

    @classmethod
    def construct_instance(cls, profile, params):
        try:
            # If an id is passed, update the obj
            id_ = from_global_id(params.pop('id'))[1]
            obj = cls._meta.model.objects.filter(profile=profile, id=id_).first()
            if not obj:
                raise cls._meta.model.DoesNotExist('That id does not exist')
        except KeyError:
            # otherwise create an obj.
            obj = cls._meta.model()
            obj.profile = profile

        return obj

    @classmethod
    def save(cls, instance, **kwargs):
        for x, y in kwargs.items():
            setattr(instance, x, y)
        instance.save()
        return instance

    @classmethod
    def mutate(cls, root, info, **kwargs):
        # FIXME the root argument is self but is currently being passed as None
        """
        As per the v2.0 upgrade docs, mutate should no longer be a classmethod. But
        It doesn't pass an instance as self, but rather None. Either I approach this all
        wrong or it is a bug in graphene.
        """
        info__context_user = info.context.user

        if not info__context_user.is_authenticated:
            return None

        if cls._input_valid(kwargs):
            try:
                obj = cls.construct_instance(info__context_user, kwargs)
            except cls._meta.model.DoesNotExist:
                return None
            params = cls.handle_relation_fields(kwargs)
            params = cls.extra_handlers(info__context_user, params)
            return cls.save(obj, **params) if cls._input_valid(params) else None
        else:
            return None

    @classmethod
    def extra_handlers(cls, user, opts):
        """
        By default this doesn't do anything, override this
        in the subclass.
        This might be errorprone, since the extra handlers are not guaranteed
        to be clean data

        :param user:
        :param opts:
        :return: the kwargs to be used for the creation
        """
        return opts

    @classmethod
    def handle_relation_fields(cls, opts):
        """
        We will decrypt the id and add this to opts

        :param instance:
        :param opts:
        :return: opts
        """
        relation_fields = cls.get_relation_fields_by_meta(cls._meta)
        relations_to_create = [
            cls.convert_relational_name(x)
            for x in relation_fields
            if cls.convert_relational_name(x) in opts.keys()
        ]

        for relation in relations_to_create:
            rid = from_global_id(opts[relation])
            opts[relation] = rid[1]

        return opts

    @classmethod
    def _input_valid(cls, opts):
        model_fields = cls._meta.model._meta.get_fields()

        # Check if all non_nullable_fields are present
        non_nullable_fields = \
            list(
                filter(
                    lambda f:
                    not f.is_relation
                    and not f.primary_key
                    and not f.blank
                    and f not in cls._meta.excluded_fields,
                    model_fields,
                )
            )

        for field in non_nullable_fields:
            if field.name not in opts:
                return None

        return True


class DeleteModelMutation(ModelMutation):
    class Meta:
        abstract = True

    @classmethod
    def __init_subclass_with_meta__(cls, **options):
        """
        Filter fields is unused, but the superclass ModelMutation requires filter_fields
        for all subclasses. Now, we could change the existing ModelMutation to not make this
        a required field, but I prefer to not do this.
        My first thought was that I could pass the filter_fields in the Meta class, but
        since this is an abstract class, it cannot pass anything but 'abstract'
        :param options:
        :return:
        """
        options.setdefault('filter_fields', {})
        options.setdefault('include_level', INCLUDE_LEVELS.NONE)
        super().__init_subclass_with_meta__(**options)

    @classmethod
    def mutate(cls, root, info, **kwargs):
        info__context_user = info.context.user

        if not info__context_user.is_authenticated:
            return None

        try:
            id_ = from_global_id(kwargs.pop('id'))[1]
            obj = cls._meta.model.objects.filter(profile=info__context_user, id=id_).first()
            if not obj:
                raise cls._meta.model.DoesNotExist('This id is invalid')
            obj.delete()
            return cls(status=200)
        except (KeyError, binascii.Error, cls._meta.model.DoesNotExist, cls._meta.model.MultipleObjectsReturned):
            return None
