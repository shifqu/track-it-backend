from graphene import Node
from graphene_django.types import DjangoObjectTypeOptions, DjangoObjectType


class BaseDjangoObjectTypeOptions(DjangoObjectTypeOptions):
    field_filters = None  # Type: dict
    field_matrix = None  # Type: dict


class BaseDjangoObjectType(DjangoObjectType):
    class Meta:
        abstract = True

    @classmethod
    def __init_subclass_with_meta__(cls, _meta=None, field_filters=None, **options):
        # Provide a default value for interfaces
        try:
            options['interfaces']
        except KeyError:
            options['interfaces'] = (Node, )

        assert isinstance(field_filters, dict),  (
            "The attribute field_filters in {} needs to be an instance of "
            'dict, received "{}".'
        ).format(cls.__name__, field_filters)

        if not _meta:
            _meta = BaseDjangoObjectTypeOptions(cls)

        _meta.field_filters = field_filters
        _meta.field_matrix = cls.create_field_matrix(field_filters)

        super().__init_subclass_with_meta__(_meta=_meta, **options)

    @staticmethod
    def create_field_matrix(input_dict):
        d = dict()
        for k, v in input_dict.items():
            d[k] = v[0]
            for f in v[1]:
                d['__'.join([k, f]).lower()] = v[0]
        return d
